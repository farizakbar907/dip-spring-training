package com.example.training.repository;


import com.example.training.models.Employee;
import com.example.training.viewmodel.EmployeeAddress;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Repository
public class EmployeeRepository {

    public List<Employee> findAllEmployee(){
        // Get Curren
        Date date = new Date();

        // Instance & Set Value Object Employee
        Employee dataEmployee = new Employee();

        // Set Dummy Value Employee
        List<Employee> returnValue = new ArrayList<>();
        for(int i = 0; i < 10; i++){
            dataEmployee.setName("User " + i);
            dataEmployee.setId("UID " + i);
            dataEmployee.setPhone("Phone Number " + i);
            dataEmployee.setEmail("Mail Sample " + i);
            dataEmployee.setAddress("City " + i);

            returnValue.add(i, dataEmployee);
        }
        return returnValue;
    }

    public List<Employee> findEmployeeById(String idEmployee){
        return null;
    }

    public Integer insertEmployeeAddress(EmployeeAddress dataForSave){
        // Luas Bangunan
        // Warna Rumah
        // Jumlah Mobil
        // Ingin menggunakan model Employee tapi ditambah Atribute diatas yang tidak ada di model Employee
        // gunakan viewmodel

        dataForSave.setHouseColor("Merah");

        return null;
    }

    public Integer insertNewEmployee(Employee dataForSave){

        UUID uuid = UUID.randomUUID();
        dataForSave.setName("Nama");
        dataForSave.setPhone("Asd");

        return null;
    }

    public Integer updateEmployee(Employee dataForSave){
        return null;
    }

    public Integer deleteEmployee(String idEmployee){
        return null;
    }

    public Integer deleteAllEmployee(){
        return null;
    }

    public Integer insertEmployee(Employee dataForSave){
        return null;
    }
}

